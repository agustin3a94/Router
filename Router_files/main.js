var socket = io();
  
angular.module('router', [])
  .controller('routerCtrl', function($scope) {

    $scope.columns = [];
    $scope.rows = [];

    socket.on('dv', function (data) {
      console.log(data);
      parseTable(data.table);
    });

    var parseTable = function(table) {
      var col = [];
      var rows = [];
      for(var key in table) {
        col.push(key);
        rows.push(table[key]);
      }
      //console.log(table);
      //console.log(col);
      $scope.nodes = table;
      $scope.columns = col;
      $scope.$apply();
    };

    socket.on('messages', function(messages) {
      $scope.messages = messages.messages;
      console.log(messages);
      $scope.$apply();
    });

    setInterval(function() {
      socket.emit('getMessages');
      socket.emit('getDv');
    },1000);    

    $scope.sendMessage = function() {
      socket.emit('message', { dest: $scope.message.to, message: $scope.message.text});
    };

  });