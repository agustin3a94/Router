var DV = require('./lib/dv/distanceVector.js');
		config     = require('./lib/utils/readConfig.js'),
		config     = config.readConfig();

	//dv.from -> B
	// dv.costs ==
	// [ { node : 'A', cost: 15 } , { node : 'C', cost: 90} ]

var b = {};
b.from = 'B';
b.costs = [ { node : 'A' , cost: 3}, { node : 'D' , cost: 2}];

var dv = new DV(config.nodes);

dv.printTable();

console.log(dv.handleDV(b));

dv.printTable();

var c = {};
c.from = 'C';
c.costs = [ { node : 'A' , cost: 23}, { node : 'D' , cost: 5}];

console.log(dv.handleDV(c));

dv.printTable();

dv.calculateMin('D');


console.log('setToInfinite TO B');

dv.setToInfinite('B');

dv.printTable();

dv.setToDefault('B');

dv.printTable();

console.log(dv.dvString());

/*
console.log(dv.handleDV(b));




*/

/*


b.costs = [ { node : 'A' , cost: 3}, { node : 'D' , cost: 99}];

*/

//console.log(b.costs); 
//console.log(dv.handleDV(b));

//dv.printTable();

//dv.printTable();