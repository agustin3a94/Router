var config     = require('./lib/utils/readConfig.js'),
		Db         = require('./lib/db/dbconnection.js'),
		Dv				 = require('./lib/dv/distanceVector.js'),
		// Routing
		Routing    = require('./lib/routing/routing.js'),
		RSocket = require('./lib/routing/RSocket.js'),
		// Forwarding 
		Forwarding = require('./lib/forwarding/forwarding.js'),
		FSocket		 = require('./lib/forwarding/FSocket.js'),
		// App
		App 			 = require('./lib/app/index.js'),
		debug      = require('debug')('main'),
		config     = config.readConfig(),
		net        = require('net'),
		node;

/// INIT 
global.db = new Db();
global.dv = new Dv(config.nodes);
global.router = new Routing();
global.forward = new Forwarding();
global.name = config.name;
var rserver = net.createServer(function(socket) {
	new RSocket(socket,'server');
});
var fserver = net.createServer(function(socket) {
	new FSocket(socket,'server');
});
//INIT DB
global.db.init()
.then(function() {
//SAVE NODES
	return global.db.saveNodes(config.nodes);
}).then(function() {
//INIT SERVER
	debug("Init db");
	rserver.listen(9080, function() {
		debug('RServer listen in port 9080');
	});
	fserver.listen(1981, function() {
		debug('FServer listen in port 1981');
	});
	return global.db.getNodes();
}).then(function(nodes) {
//OPEN CONNECTIONS
	for(var i = 0; i < nodes.length; i++) {
		node = nodes[i];
		global.router.addConnection(node.name, new RSocket(null,'client',node));
		global.forward.addConnection(node.name, new FSocket(null,'client',node));
	}
	global.router.resetConnections();
}).catch(function(err) {
	debug(err);
});
new App();