var debug = require('debug')('DV'),
	config = require('../utils/readConfig.js'),
	config = config.readConfig();
	global.name = config.name;

var method = distanceVector.prototype;
var initial = new Array();
var distanceList = new Array();
var minS = new Object();
minS.Dv = new Array();
var myName = config.name;

function distanceVector(nodes){
	this.tableDV = {};
	this.defaultCosts = {};
	for(var i = 0; i < nodes.length; i++) {
		this.tableDV[nodes[i].name] = {};
		this.tableDV[nodes[i].name][nodes[i].name] = nodes[i].cost;
		this.tableDV[nodes[i].name].min = nodes[i].name;
		this.defaultCosts[nodes[i].name] = nodes[i].cost;
	};
}

method.handleDV = function(nodes) {
	var from = nodes.from;
	var costs = nodes.costs;
	var node, cost, ncost;
	// GET COST FOR FROM NODE
	var fromCost = this.getMin(from);
	var changes = false;
	// CHECK IF FROM-NODE EXIST
	if(fromCost.node != undefined) {
		for(var i = 0; i < costs.length; i++) {
			node = costs[i].node;
			cost = costs[i].cost;
			debug("node " + node + " cost: " + cost);
			cost = costs[i].cost + fromCost.cost;
			cost = (cost > 99)? 99 : cost;
			// IF NODE IN LIST IS THIS ROUTER DO NOTHING
			if(node != global.name) {
				// IF NODE DOESN'T EXIST ADD TO TABLE
				if((this.tableDV[node] == undefined)) {
					this.tableDV[node] = {};
					this.tableDV[node][from] = cost;
					this.tableDV[node].min = from;
					changes = true;
				} else {
					ncost = this.getMin(node);
					this.tableDV[node][from] = cost;
					// IF THE CURRENT COST IS LESS THAN MINCOST CHANGE MIN NODE
					if(cost < ncost.cost) {
						this.tableDV[node].min = from;
						changes = true;
					} else if((from == ncost.node) && (cost > ncost.cost)) {
						this.calculateMin(node);
						changes = true;
					}
				}
			}
		}
	}
	return changes;
}

method.setToInfinite = function(name) {
	debug('Set to infinite ' + name);
	if(this.tableDV[name] != undefined) {
		if(this.tableDV[name][name] != undefined) {
			this.tableDV[name][name] = 99;
			this.calculateMin(name);
		}
	}
}

method.setToDefault = function(name) {
	if(this.tableDV[name] != undefined) {
		if(this.tableDV[name][name] != undefined) {
			this.tableDV[name][name] = this.defaultCosts[name];
			this.calculateMin(name);
		}
	}
}

method.calculateMin = function(name) { 
	var cCost, minNode;
	var minCost = 99;
	for (var key in this.tableDV[name]) {
		if(key != 'min') {
			cCost = this.tableDV[name][key];
			if(cCost <= minCost) {
				minCost = cCost;
				minNode = key;
			}
		}
	}
	this.tableDV[name].min = minNode;
}

method.getMin = function(name) {
	var r = {};
	if(this.tableDV[name] != undefined) {
		r.node = this.tableDV[name].min;
		r.cost = this.tableDV[name][r.node];
	}
	return r;
}

method.getTable = function() {
	return this.tableDV;
}

method.dvString = function() {
	var r = "";
	var l = 0,
	min;
	for(var key in this.tableDV) {
		min = this.tableDV[key].min;
		if(l == 0) {
			r = key + ":" + this.tableDV[key][min];
		} else {
			r = r + "\n" + key + ":" + this.tableDV[key][min];
		}
		
		l++;
	}
	return { length : l, string: r};
}

method.printTable = function() {
	console.log(this.tableDV);
}

module.exports = distanceVector;