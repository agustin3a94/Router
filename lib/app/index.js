var app  = require('express')(),
		http = require('http').Server(app),
		io   = require('socket.io')(http),
		debug= require('debug')('app'),
		dest,message;

var method = App.prototype;

function App() {



	http.listen(1337, function() {
		debug("App server listening on 1337")
	});

	app.get( '/', function( req, res ){
    res.sendFile( '/index.html' , { root:__dirname });
	});

	app.get( '/*' , function( req, res, next ) {
	    var file = req.params[0];
	    res.sendFile( __dirname + '/' + file );
	});

	io.on('connection', function (app) {
		var dvT;
		dvT = global.dv.getTable();
		app.emit('dv', { table : dvT });

		app.on('getDv', function() {
			dvT = global.dv.getTable();
			app.emit('dv',{ table : dvT});
		});

		app.on('message', function(data) {
			dest = data.dest;
			message = data.message; 
			global.forward.send(global.name, dest, message);
		});

		app.on('getMessages', function() {
			global.db.getMessages()
			.then(function(messages) {
				app.emit('messages', { messages : messages});
			}).catch(function(err) {
				debug('ERROR ' + err);
			});
		});

		global.db.on('newMessage', function(message) {
			app.emit('newMessage', message);
		});

		/*
		setInterval(function() {
			dvT = global.dv.getTable();
			app.emit('dv', { table : dvT });
		},2000);
		*/
	});

}

module.exports = App;

