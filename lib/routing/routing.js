var debug = require('debug')('routing');

var method = Routing.prototype;

function Routing() {
		this.connections = [];
}

method.handleDV = function(dv) {
	// for(var i = 0; dv.array.length; i++)
	// EXAMPLE
	// dv.from -> B
	// dv.costs ==
	// [ { node : 'A', cost: 45 } , { node : 'C', cost: 90} ]

	if(global.dv.handleDV(dv)) {
		debug("CHANGES IN DV");
		for (var key in this.connections) {
			this.connections[key].dv = true;
		}
	}
}

method.resetDv = function() {
	for (var key in this.connections) {
		this.connections[key].dv = true;
	}
}

method.getDvStatus = function(name) {
	var r = false;
	if(this.connections[name] != undefined) {
		r = this.connections[name].dv;
	}
	return r;
};

method.setDvStatus = function(name,status) {
	if(this.connections[name] != undefined) {
		this.connections[name].dv = status;
	}
};

method.addConnection = function(name,connection) {
	this.connections[name] = { dv : true , connection : connection, phase: 0};
	debug("Add connection for node " + name);
}

method.resetConnections = function() {
	var node;
	setInterval(function() {
		debug("Try reconnections");
		global.db.getDisconnected()
		.then(function(nodes) {
			for(var i = 0; i < nodes.length; i++) {
				node = nodes[i];
				if(this.connections[node.name] != undefined) {
					this.connections[node.name].connection.reconnect();
				}
			}
		}.bind(this));
	}.bind(this),30000);
}

method.send = function(message,dest) {
	if(this.connections[dest] != undefined) {
		if(message == 'WELCOME') {
			response = "From:" + global.name + "\nType:WELCOME\n\r\r";
			debug("Send Message: " + message + " to " + dest);
			this.connections[dest].connection.sendMessage(response);
		}
	}
	//socket.write(response);	
}

module.exports = Routing;