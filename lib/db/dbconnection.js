var Promise = require('bluebird');
var events = require('events');
var sqlite3 = require('sqlite3');
var db = new sqlite3.Database('db.db');

var method = Db.prototype;
method.__proto__ = events.EventEmitter.prototype;

function Db() {
	events.EventEmitter.call(this);
}

method.init = function() {
	return new Promise(function(done,fail) {
		var setConfig = true;
		try { 
			db.run("DROP TABLE if exists nodes");
		} catch(e) {}
		db.run("CREATE TABLE if not exists nodes (id INTEGER PRIMARY KEY AUTOINCREMENT, name STRING, ip STRING, cost INTEGER, status INTEGER )");
		db.run("CREATE TABLE if not exists messages (id INTEGER PRIMARY KEY AUTOINCREMENT, sender STRING, text STRING)");
		//db.run("CREATE TABLE if not exists dv (id INTEGER PRIMARY KEY AUTOINCREMENT, name STRING, cost INTEGER, via STRING");
		// TODO CREATE TABLE DV
		done();
	});
}

// NODES & CONNECTION

method.saveNodes = function(nodes) {
	return new Promise(function(done,fail) {
		if(nodes.length > 0) { 
			var query = "INSERT INTO nodes (name,ip,cost,status) VALUES";
			for(var i = 0; i < nodes.length; i++) {
				if(i != 0) {
					query = query + " ,";
				}
				query = query + " ('" + nodes[i].name + "', '" + nodes[i].ip + "', " + nodes[i].cost + ", 0)"; 
			}
			db.run(query, {}, function(err) {
				if(err) {
					fail(err);
				} else {
					done();
				}
			});
		} else {
			done();
		}
	});
}

method.saveNode = function(name,ip,cost,status) {
	return new Promise(function(done,fail) {
		db.run("INSERT INTO nodes (name, ip, cost, status) VALUES ($name, $ip, $cost, $status)", {
			  $name: name,
			  $ip: ip,
			  $cost : cost,
			  $status : status
			}, function(err) {
				if(err) {
					fail(err);
				} else {
					done();
				}
		});
	});
}

method.getNodes = function() {
	return new Promise(function(done,fail) {
		db.all("SELECT * FROM nodes",{},function(err,nodes) {
			if(err) { fail(err) }
			else {
				done(nodes);
			}
		});
	});
}

method.getNode = function(name) {
	return new Promise(function(done,fail) {
		db.get("SELECT * FROM nodes WHERE name='$name'",{ $name : name } , function(err,row) {
			if(err) { fail(err) }
			else {
				 done(row);
			}
		});
	});
}

method.getDisconnected = function() {
	return new Promise(function(done,fail) {
		db.all("SELECT * FROM nodes WHERE status=0",{},function(err,nodes) {
			if(err) { fail(err) }
			else {
				done(nodes);
			}
		});
	});
}

method.getStatus = function(name) {
	return new Promise(function(done,fail) {
		db.get("SELECT status FROM nodes WHERE name=$name", { $name : name }, function(err,row) {
			if(err) { fail(err) }
			else {
				done(row.status);
			}	
		});
	});
}

method.updateStatus = function(name,status) {
	return new Promise(function(done,fail) {
		db.run("UPDATE nodes SET status=$status WHERE name=$name", { $name:name, $status:status}, function(err) {
			if(err) { fail(err) }
			else {
				done();
			} 
		});
	});
}

// -->


// MESSAGES

method.saveMessage = function(sender,text) {
	return new Promise(function(done,fail) {
		db.run("INSERT INTO messages (sender, text) VALUES ($sender, $text)", {
				$sender : sender,
				$text : text
			}, function(err) {
				if(err) { 
					fail(err) 
				} else  {
					this.emit('newMessage',{ sender: sender, text: text});
					done();
				}
			}
		);
	});
}

method.getMessages = function() {
	return new Promise(function(done,fail) {
		db.all("SELECT * FROM messages",{},function(err,messages) {
			if(err) { fail(err) }
			else {
				done(messages);
			}
		});
	});
}

module.exports = Db;