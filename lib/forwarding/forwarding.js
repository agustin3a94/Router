var debug = require('debug')('forwarding');

var method = Forwarding.prototype;

function Forwarding() {
		this.connections = [];
}

method.addConnection = function(name,connection) {
	this.connections[name] = {status : false, connection: connection, buffer : []};
	debug("Add connection for node " + name);
}

method.setStatus = function(name,status) {
	if(this.connections[name] != undefined) {
		this.connections[name].status = status;
	}
}

method.getStatus = function(name) {
	var r = false;
	if(this.connections[name] != undefined) {
		r = this.connections[name].status;
	}
	return r;
}

method.getBuffer = function(name) {
	var r = null;
	if(this.connections[name] != undefined) {
		r = this.connections[name].buffer;
	}
	return r;
}

method.send = function(from,dest,message) {
	if(dest != global.name) {
		var f = global.dv.getMin(dest);
		debug(f);
		if(f.node != undefined) {
			var newMessage = "From:" + from + "\nTo:" + dest + "\nMsg:" + message + "\nEOF\n\r\r";
			debug(newMessage);
			if(this.connections[f.node] != undefined) {
				if(this.connections[f.node].status) {
					debug("Forward message to " + dest + " via " + f.node);
					this.connections[f.node].connection.sendMessage(newMessage);
				} else {
					this.connections[f.node].buffer.push(newMessage);
					debug("Save message in buffer to " + dest + " via " + f.node);
					this.connections[f.node].connection.reconnect();
				}
			}
		}
	} else {
		global.db.saveMessage(from,message);
		debug("Message for me");
	}
}

module.exports = Forwarding;