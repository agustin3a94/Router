var net    = require('net');
var msgExp = new RegExp(/\s*From\s*:\s*([a-zA-Z|0-9]+)\s*To\s*:\s*([a-zA-Z|0-9]+)\s*msg\s*:([\S|\s]*)EOF/i);
var debug  = require('debug')('FSocket');
var method = FSocket.prototype;

function FSocket(socket,type,node) {
	this.type = type;
	if(this.type == 'client') {
		this.ip = node.ip;
		this.name = node.name;
  	this.socket = new net.Socket();
  	this.socket.on('error', function(err) {
  		debug(this.name + " " + err);
  		this.handleDisconnection(false);
  	}.bind(this));
  	this.socket.on('close', function(err) {
  		this.handleDisconnection(err);
  	}.bind(this));
  	this.socket.on('end', function() {
  		this.handleDisconnection(false);
  	}.bind(this));
  	this.socket.on('connect', function() {
  		this.handleConnection();
		}.bind(this));
		this.socket.connect(1981, this.ip);
	} else {			
		this.socket = socket;
		this.socket.on('error', function(err) {
  		debug(err);
  	}.bind(this));
	}

	this.socket.on('data', function(data) {
		var str = data.toString('utf8');
		this.handleMessage(str);		
	}.bind(this));
		
}

method.handleMessage = function(message) {
	console.log(message);
		var matcher = message.match(msgExp),
				origin,dest,m;
		if(matcher != null) {
			debug(matcher);
			origin = matcher[1];
			dest = matcher[2];
			m = matcher[3];
			debug('Message from: ' + origin + " To: " + dest + " message:" + m);
			global.forward.send(origin,dest,m);
		}
};

method.handleConnection = function() {
	global.forward.setStatus(this.name,true);
	var m;
	//var c = setInterval(function(){ myTimer() }, 1000);
	//var myTimer = function() {
  //	if(global.forward.connections[this.name].buffer.length != 0) {
  //		m = global.forward.connections[this.name].buffer.shift();
	//		this.socket.write(m);
  //	} else {
  //		clearInterval(c);
  //	}
	//}.bind(this);
	m = global.forward.connections[this.name].buffer.shift();
	if(m) {
		this.socket.write(m);
	}
};

method.handleDisconnection = function(err) {
	global.forward.setStatus(this.name,false);
};

method.reconnect = function() {
	this.socket.connect(1981, this.ip);
};

method.sendMessage = function(message) {
	this.socket.write(message);
};

module.exports = FSocket;