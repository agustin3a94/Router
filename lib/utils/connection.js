var net = require('net');

var fromHello     = new RegExp(/\s*From\s*:\s*([a-zA-Z|0-9]+)\s*Type\s*:\s*HELLO\s*/i),
		fromKeepAlive = new RegExp(/\s*From\s*:\s*([a-zA-Z|0-9]+)\s*Type\s*:\s*KeepAlive\s*/i),
		fromWelcome		= new RegExp(/\s*From\s*:\s*([a-zA-Z|0-9]+)\s*Type\s*:\s*WELCOME\s*/i),
		from          = new RegExp(/\s*From\s*:(.*)\s*/),
		typeHello     = new RegExp(/\s*Type\s*:\s*HELLO\s*/),
		typeKeepAlive = new RegExp(/\s*From\s*:\s*KeepAlive/),
		fromDv = new RegExp(/\s*From\s*:\s*([a-zA-Z|0-9]+)\s*Type\s*:\s*(DV)\s*\s*LEN:\s*([0-9]+)\s*(([a-zA-Z|0-9]+)\s*:\s*([0-9]+)\s*)*\s*/i),
		typeDv = new RegExp(/\s*Type\s*:\s*DV/i),
		lenDv = new RegExp(/\s*Len\s*:\s*[0-9]*/i),
		tableDv = new RegExp(/\s*(([a-zA-Z|0-9]+)\s*:\s*([0-9]+)\s*)*\s*/i);

var debug = require('debug')('connection');
	//DV = require('./lib/dv/distanceVector.js');
var method = Connection.prototype;
//var distVector = new DV();
function Connection(socket,type,node) {
  	this.chunk = false;  	
  	this.type = type;
  	this.disconnect = false;
  	this.buffer = [];
  	if(this.type == 'client') {
  		this.ip = node.ip;
  		this.name = node.name;
	  	this.socket = new net.Socket();
	  	this.socket.on('error', function(err) {
	  		debug(this.name + " " + err);
	  		this.handleDisconnection();
	  	}.bind(this));
	  	this.socket.on('close', function() {
	  		this.handleDisconnection();
	  	}.bind(this));
	  	this.socket.on('end', function() {
	  		this.handleDisconnection();
	  	}.bind(this));
	  	this.socket.on('connect', function() {
	  		this.handleConnection();
			}.bind(this));
			this.socket.on('timeout', function() {
				this.handleTimeout();
			}.bind(this));
			this.socket.setTimeout(10000);	  	
			this.socket.connect(9080, this.ip);
		} else {
			this.socket = socket;
			this.socket.setTimeout(10000);
			this.socket.on('error', function(err) {
	  		debug(err);
	  	}.bind(this));
	  		this.socket.on('data', function(data) {
				var str = data.toString('utf8');
				this.handleMessage(str);		
				}.bind(this));
		}		
}

method.handleMessage = function(message) {
		console.log(message); 
		var MfromHello = message.match(fromHello),
				Mfrom      = message.match(from),
				MtypeHello = message.match(typeHello),
				MfromKeepAlive = message.match(fromKeepAlive),
				MtypeKeepAlive = message.match(typeKeepAlive),
				MfromWelcome = message.match(fromWelcome),
				MfromDv = message.match(fromDv),
				MtypeDv = message.match(typeDv),
				MlenDv = message.match(lenDv),
				MDistanceVector = message.match(tableDv),
				dvLength, cost,
				originNode;

			if(MfromKeepAlive != null) {
				originNode = MfromKeepAlive[1];
				this.name = originNode;
				debug('KeepAlive from ' + originNode);
			} else if(MfromHello != null) {
				originNode = MfromHello[1];
				this.name = originNode;
				debug('HELLO from ' + originNode);
				global.router.send("WELCOME",originNode);
			} else if(MfromWelcome != null) {
				originNode = MfromWelcome[1];
				this.name = originNode;
				debug('WELCOME from ' + originNode);
			} else if(MfromDv != null){
				originNode = MfromDv[1];
				dvLength = MfromDv[3];
				this.name = originNode;
				var functionDv = new Object();
				functionDv.from = originNode;
				functionDv.costs = new Array();
				for(var i = 0; i < dvLength;i++) {
					try {
					cost = MfromDv[4].split(":")[1];
					cost = parseInt(cost);
					functionDv.costs.push({node: MfromDv[4].split(":")[0], cost: cost});
					message = message.split(MfromDv[4])[0];
					MfromDv = message.match(fromDv);
					} catch(ex) {
						debug(ex);
					} 
				}
				debug(functionDv);
				global.router.handleDv(functionDv);
			}
};

method.handleConnection = function() {
	var m;
	this.disconnect = false;
	global.db.updateStatus(this.name,1)
	.then(function() {
		debug("NODE " + this.name + " CONNECTED");
		//var c = setInterval(function(){ myTimer() }, 1000);
		//var myTimer = function() {
    //	if(this.buffer.length != 0) {
    //		m = this.buffer.shift();
		//		this.socket.write(m);
    //	} else {
    //		clearInterval(c);
    //	}
		//}.bind(this);
		debug("Send HELLO to " + this.name);
		this.socket.write('From:' + global.name + '\nType:HELLO\n\r\r');
	}.bind(this)).catch(function(err) {
	  debug("ERROR " + this.name + " " + err);
	}.bind(this));
};

method.handleDisconnection = function() {
	if(!this.disconnect) {
		this.disconnect = true;
		global.db.updateStatus(this.name,0)
		.then(function() {
			global.router.setDvStatus(this.name,true);
			global.dv.setToInfinite(this.name);
			debug("NODE " + this.name + " DISCONNECT");
		}.bind(this)).catch(function(err) {
		  debug("ERROR " + this.name + " " + err);
		  console.log(err);
		}.bind(this));
	}
};

method.handleTimeout = function() {
	if(this.type == 'client') {
		//console.log(global.router.getDvStatus(this.name));
		if(global.router.getDvStatus(this.name)) {
			//global.dv.prettyPrint();
			global.router.setDvStatus(this.name,false);
			var s = global.dv.tableDv();
			debug("Sending DV to " + this.name);
			this.socket.write("From:" + global.name + "\nType:DV\n" + "Len:"+s.Dv.length + s.string);
		} else {
			this.socket.write("From:" + global.name + "\nType:KeepAlive\n");			
		}

		// TODO SI HAY CAMBIOS EN LA TABLA DE DV AVISAR
	} else {
		debug("Timeout: " + this.name);
		global.dv.setToInfinite(this.name);
		//TODO COSTO DE NODO INFINITO
	}
};

method.reconnect = function() {
		this.socket.setTimeout(10000);  	
		this.socket.connect(9080, this.ip);
};

method.sendMessage = function(message) {
	global.db.getStatus(this.name)
	.then(function(status) {
		if(status == 1) {
			this.socket.write(message);
		} else {
			this.buffer.push(message);
			this.reconnect();
		}
	}.bind(this)).catch(function(err) {
		debug("ERROR " + err);
	});
};

module.exports = Connection;